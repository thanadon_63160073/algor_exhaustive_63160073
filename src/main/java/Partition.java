
import java.util.ArrayList;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Acer
 */
public class Partition {

    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>(Arrays.asList(3, 4, 11, 4));
        ArrayList<ArrayList> subsets = new ArrayList<ArrayList>();
        subsets = getAllSubset(arr);
        findSameSum(subsets);
    }

    public static ArrayList<ArrayList> getAllSubset(ArrayList<Integer> set) {
        ArrayList<ArrayList> subsets = new ArrayList<ArrayList>();
        int n = set.size();
        for (int i = 0; i < (1 << n); i++) {
            ArrayList<Integer> subset = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) > 0) {
                    subset.add(set.get(j));
                }
            }
            subsets.add(subset);
        }
        return subsets;
    }
    public static void findSameSum( ArrayList<ArrayList> subsets){
        int n = subsets.size();
        for(int i=0; i<n;i++){ 
            ArrayList<Integer> subset1 = subsets.get(i);
            for(int j = i; j<n; j++){
                ArrayList<Integer> subset2 = subsets.get(j);
                if(subset1.size()==0 || subset2.size()==0){
                    continue;
                }
                if(equalSet(subset1, subset2)){
                    continue;
                }
                int sum1 = sumSet(subset1);
                int sum2 = sumSet(subset2);
                if(sum1==sum2){
                    System.out.println("Found two disjoint subsets with the same sum.");
                    System.out.println(subset1+" = "+sum1);
                    System.out.println(subset2+" = "+sum2);
                    return;
                }
            }
        }
        System.out.println("Not found.");
    }
    public static int sumSet(ArrayList<Integer> set){
        int n = set.size();
        int sum = 0;
        for(int i = 0; i<n; i++){
            sum+=set.get(i);
        }
        return sum;
    }
    public static boolean equalSet(ArrayList<Integer> set1, ArrayList<Integer> set2){
        if(set1.size()!=set2.size()){
            return false;
        }
        int equal = 0;
        for(int n1:set1){
            for(int n2:set2){
                if(n1==n2){
                    equal++;
                }
            }
        }
        if(equal==set1.size()){
            return true;
        }
        return false;
    }
}
